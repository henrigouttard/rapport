#!/bin/bash
CONFIGFS_HOME="/sys/kernel/config"

[...]

echo "Creating a HID Touchscreen USB Gadget in configFS"

modprobe libcomposite
mkdir -p $CONFIGFS_HOME/usb_gadget/g1
cd $CONFIGFS_HOME/usb_gadget/g1

if [ "$verbose" = "verbose" ]; then
	echo "configFS home directory: $CONFIGFS_HOME"
fi

echo 0x1234 > idVendor 
echo 0x5678 > idProduct 
echo 0x0000 > bcdDevice 

mkdir -p strings/0x409

echo "Manufacturer" > strings/0x409/manufacturer 
echo "Product" > strings/0x409/product 

# configuration creation
mkdir -p configs/c.1/strings/0x409
echo "Configuration 1, gadget HID Touchscreen" > configs/c.1/strings/0x409/configuration 
echo 250 > configs/c.1/MaxPower 

# function creation
mkdir -p functions/hid.usb0
echo 0 > functions/hid.usb0/protocol
echo 0 > functions/hid.usb0/subclass
echo 64 > functions/hid.usb0/report_length
echo -ne \\x05\\x0d\\x09 ... > functions/hid.usb0/report_desc

# link function with config
ln -s functions/hid.usb0 configs/c.1

if [ "$verbose" = "verbose" ]; then
	echo "ConfigFS content :"
	ls -R $CONFIGFS_HOME/usb_gadget/g1
fi

ls /sys/class/udc > UDC

echo "Gadget has been set up"