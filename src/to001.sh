#!/bin/bash

# This script run touchscreen tests 
# Description : This script consists in both :
# - sending pre-recorded samples of data from a USB HID Touchscreen Gadget emulated on a Raspberry Pi Zero W to the Info 2.
# - recording the dates of data emission, and events reception in two output files in order to compare the result a posteriori. 

# Testing with ts001
# ==================
# Thanks to the micro B on the go slot, it is possible to use the Pi Zero W as a USB gadget device (as opposed to a host device, as usual).
#
# Extra requirements
# ------------------
#  - 1 external Raspberry Pi Zero W (with micro-SD and PSU)
#  - 1 USB type A to micro-USB type B cable
#
# Pi Zero W setup
# ---------------
# First it is necessary to install Linux ( > Kernel 4.4 ) on the raspberry Pi Zero W. The Raspbian Lite image is perfectly suitable.
#
# Then we need to setup the network and with a specific hostname (e.g. *ts-pi0w*). The Pi Zero needs to be connected to the test-box machine.
# You should be able to ping the Pi Zero from the test-box machine.
#
# Enable the **dwc2** device tree overlay, to do this: add the following line to `/boot/config.txt` on the Pi Zero W SD-card (boot partition).
#
#		dtoverlay=dwc2
#
# Enable the **dwc2** and **libcomposite** modules. Add these two lines to `/etc/modules` on the Pi Zero SD-card (root partition).
#
#		dwc2
#		libcomposite
#
# **Note:** The dwc2 module is an upstream driver which manages the USB On-The-Go host/gadget flip. The libcomposite module is a driver which allows user to configure USB gadget, thanks to the ConfigFS filesystem provided by the kernel.
#
# Now that the Pi Zero W is set, we can set TM_HAS_PI_ZERO variable
#
#		export TM_HAS_PI_ZERO=1
#
# Additional set up on ICE
# ------------------------
# Set up environment:
#
#     export PI_ZERO_W=ts-pi0w
# (use here the hostname you have set on the Pi Zero W)
#
# Script start
# ------------
# **WARNING** be sure to have plug the Pi Zero if you want to run the script during the night.
#
# Once the setup is done you can launch the touchscreen test with the same way that other tests :
#
#     ./tests/to001.sh
#
# You can also specifie the number of touchscreen touch which will be simulated (default is 5) :
#
#     SEQ_COUNT=1000 ./tests/to001.sh
# (for 1000 simulated touch)
. lib/test_manage.sh
TID="$(basename $0 '.sh')"
tm_mkoutdir

BIN_EVTEST="bin/evtest"
TARGET_EVTEST_DIR="/tmp/evtest"
TARGET_EVTEST="$TARGET_EVTEST_DIR/evtest"
EVTEST_RECORD="$TARGET_EVTEST_DIR/time_record_ice"

DEV_INPUT_EVENT="/dev/input/event1"

PI_ZERO_W_TAR="ltests/$TID-gadget.tar.gz"
PI_ZERO_W_DST="/root"
PI_ZERO_W_TAR_DST="$PI_ZERO_W_DST/touchscreen_gadget.tar.gz"
PI_ZERO_W_DIR="$PI_ZERO_W_DST/touchscreen_gadget"

PI_ZERO_W_SETUP="$PI_ZERO_W_DIR/setup.sh"
PI_ZERO_W_SEND_PACKET="$PI_ZERO_W_DIR/send_packets.sh"
PI_ZERO_W_PACKETS_DIR="$PI_ZERO_W_DIR/frames/mus"
PI_ZERO_W_RECORD="$PI_ZERO_W_DIR/time_record_gadget"

TM_NEED_PI_ZERO=1
TM_DEPS "$BIN_EVTEST"

if [ -z $SEQ_COUNT ]; then
	SEQ_COUNT=5
	echo "SEQ_COUNT is not set, running test anyway, 5 sequences will be sent"
	echo "usage: SEQ_COUNT=<count> $0"
	echo
fi

gadget_exec() {
    if [ -z $PI_ZERO_W ]; then
		tm_log "PI_ZERO_W is not set."
		exit 1
    fi
	ssh -q root@$PI_ZERO_W -- $*	
}

file_install() {
	tm_log "Install evtest"
	if ! tm_exec "test -d $TARGET_EVTEST_DIR"; then
		tm_exec "mkdir -p $TARGET_EVTEST_DIR"
	fi

	if ! scp $BIN_EVTEST $ICE:$TARGET_EVTEST; then
		tm_log "Failed to install evtest"
		tm_pr_result $TID "FAIL"
		exit 1
	fi

	tm_log "Install Touchscreen Gadget files"
	if ! gadget_exec "test -d $PI_ZERO_W_DST "; then
		tm_log "$PI_ZERO_W_DST doesn't exist" >&2
		exit 1
	fi

	if ! scp $PI_ZERO_W_TAR root@$PI_ZERO_W:$PI_ZERO_W_TAR_DST; then
		tm_log "Failed to copy Touchscreen Gadget tarball" >&2
		exit 1
	fi

	if ! gadget_exec "tar -xf $PI_ZERO_W_TAR_DST";
	then
		tm_log "Failed to extract Touchscreen Gadget tarball" >&2
		exit 1
	fi
}

setup() {
	#Disable the real USB touchscreen
	tm_exec "gpio TOUCH-PWR-EN 0"

	# Setup the USB gadget
	gadget_exec "$PI_ZERO_W_SETUP"
	tm_log "Plug the USB gadget to $ICE if it is not already plugged"
	while ! tm_exec "test -e $DEV_INPUT_EVENT"; do
		sleep 1
	done
	tm_log "USB Touchscreen input device: $DEV_INPUT_EVENT"
}

send_catch() {
	# Execute evtest in the background, in order to catch events and record reception dates
	tm_exec "$TARGET_EVTEST $DEV_INPUT_EVENT > /dev/null 2>&1" &

	# Send sequences from gadget, and record emission dates
	tm_log "Send $SEQ_COUNT sequences"
	gadget_exec "$PI_ZERO_W_SEND_PACKET $PI_ZERO_W_PACKETS_DIR $SEQ_COUNT"
	tm_exec "mv time_record_ice $EVTEST_RECORD"
}


get_records() {
	# At this time two records have been generated : 
	# one on pi0w, the other on ice. We just need to get them
	tm_log "Get time records"
	scp root@$PI_ZERO_W:$PI_ZERO_W_RECORD $TOUTDIR
	scp $ICE:$EVTEST_RECORD $TOUTDIR
}


cleanup() {
	# Cleaning up
	tm_log "Cleanup"
	gadget_exec "$PI_ZERO_W_SETUP -d"
	gadget_exec "rm -r $PI_ZERO_W_DIR"
	gadget_exec "rm $PI_ZERO_W_TAR_DST"
	tm_exec "rm -r $TARGET_EVTEST_DIR"	
	tm_exec "gpio TOUCH-PWR-EN 1"
}

sus2s() {
	local sec=$1
	local usec=$2
	echo "$sec + (0.000001 * $usec)" | bc
}

delta() {
	local d_usec=$(echo "$2-$1" | bc)
	tm_stat "$3" "$d_usec,$1,$2"
}

tm_pr_start $TID

if [ -z $PI_ZERO_W ]; then
	tm_log "PI_ZERO_W is not set."
	exit 1
fi

file_install
setup
send_catch
get_records

for num_line in $(seq 1 4 $(($SEQ_COUNT*4))); do

	## gadget records
	gsec1=$(sed "${num_line}q;d" $TOUTDIR/time_record_gadget)
	gusec1=$(sed "$(($num_line+1))q;d" $TOUTDIR/time_record_gadget)
	gsec2=$(sed "$(($num_line+2))q;d" $TOUTDIR/time_record_gadget)
	gusec2=$(sed "$(($num_line+3))q;d" $TOUTDIR/time_record_gadget)

	## ice records
	isec1=$(sed "${num_line}q;d" $TOUTDIR/time_record_ice)
	iusec1=$(sed "$(($num_line+1))q;d" $TOUTDIR/time_record_ice)
	isec2=$(sed "$(($num_line+2))q;d" $TOUTDIR/time_record_ice)
	iusec2=$(sed "$(($num_line+3))q;d" $TOUTDIR/time_record_ice)

	delta $(sus2s $gsec1 $gusec1) $(sus2s $isec1 $iusec1) "SOF"
	delta $(sus2s $gsec2 $gusec2) $(sus2s $isec2 $iusec2) "EOF"

done

cleanup
